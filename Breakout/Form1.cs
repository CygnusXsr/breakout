﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;

namespace Breakout
{
    public partial class Form1 : Form
    {
        Vector ballPos;
        int ballRadius;
        Vector ballSpeed;
        Rectangle paddlePos;
        List<Rectangle> blockPos;
        Timer timer = new Timer();

        public Form1()
        {
            InitializeComponent();
            this.ballPos = new Vector(200, 200);
            this.ballRadius = 10;
            this.ballSpeed = new Vector(-2, -4);
            this.paddlePos = new Rectangle(100, this.Height - 50, 100, 5);

            this.blockPos = new List<Rectangle>();
            for (int x = 0; x <= this.Height; x += 100)
            {
                for (int y = 0; y <= 150; y += 40)
                {
                    this.blockPos.Add(new Rectangle(25 + x, y, 80, 25));
                }
            }

            //Timer timer = new Timer();
            timer.Interval = 33;
            timer.Tick += new EventHandler(Update) ;
            timer.Start();
            if (this.blockPos.Count == 0 || ballPos.Y + ballRadius >= this.Height)
            {
                timer.Stop();
            }
        }
        double DotProduct(Vector a, Vector b)
        {
            //内積計算
            return a.X * b.X + a.Y * b.Y;
        }
        bool LineVsCircle(Vector p1,Vector p2,Vector center,float radius)
        {
            //パドルの方向ベクトル
            Vector lineDir = (p2 - p1);
            //パドルの法線
            Vector n = new Vector(lineDir.Y, -lineDir.X);
            n.Normalize();
            Vector dir1 = center - p1;
            Vector dir2 = center - p2;
            double dist = Math.Abs(DotProduct(dir1, n));
            double a1 = DotProduct(dir1,lineDir);
            double a2 = DotProduct(dir2,lineDir);
            return (a1 * a2 < 0 && dist < radius) ? true : false;
        }
        int BlockVsCircle(Rectangle block, Vector ball)
        {
            if (LineVsCircle(new Vector(block.Left, block.Top),
                new Vector(block.Right, block.Top), ball, ballRadius))
                return 1;

            if (LineVsCircle(new Vector(block.Left, block.Bottom),
                new Vector(block.Right, block.Bottom), ball, ballRadius))
                return 2;

            if (LineVsCircle(new Vector(block.Right, block.Top),
                new Vector(block.Right, block.Bottom), ball, ballRadius))
                return 3;

            if (LineVsCircle(new Vector(block.Left, block.Top),
                new Vector(block.Left, block.Bottom), ball, ballRadius))
                return 4;

            return -1;
        }
        private void Update(object sender,EventArgs e)
        {
            //ボールの移動
            ballPos += ballSpeed;
            //左右の壁でバウンド
            //ballPosはボールの中心なので、ボールの半径を足すもしくは引く必要がある
            //if(右側の壁 || 左側の壁)
            //壁に当たったときにballSpeedのx座標が反転するようにしている
            if(ballPos.X + (ballRadius)*2 > this.Bounds.Width || ballPos.X - ballRadius < 0)
            {
                ballSpeed.X *= -1;
            }
            //上の壁でバウンド
            //壁に当たったときにballSpeedのy座標が反転するようにしている
            if (ballPos.Y - ballRadius < 0)
            {
                ballSpeed.Y *= -1;
            }
            //下の壁を通過した場合
            if (ballPos.Y + (ballRadius)*5 >= this.Height)
            {
                this.label2.Text = "ゲームオーバー";
                timer.Stop();
            }
            // パドルのあたり判定
            if (LineVsCircle(new Vector(this.paddlePos.Left, this.paddlePos.Top),
                             new Vector(this.paddlePos.Right, this.paddlePos.Top),
                             ballPos, ballRadius))
            {
                ballSpeed.Y *= -1;
            }
            // ブロックとのあたり判定
            for (int i = 0; i < this.blockPos.Count; i++)
            {
                int collision = BlockVsCircle(blockPos[i], ballPos);
                if (collision == 1 || collision == 2)
                {
                    ballSpeed.Y *= -1;
                    //ブロックの当たり判定がtrueだったらListのblockPosから
                    //当たったblockPosインスタンスを削除してブロックを消す
                    this.blockPos.Remove(blockPos[i]);  // 追加
                }
                else if (collision == 3 || collision == 4)
                {
                    ballSpeed.X *= -1;
                    this.blockPos.Remove(blockPos[i]); // 追加
                }
            }
            if(this.blockPos.Count == 0)
            {
                this.label2.Text = "コングラッチュレイション！！";
                timer.Stop();
            }

            //再描画
            Invalidate();
        }
        private void Draw(object sender, PaintEventArgs e)
        {
            SolidBrush purpleBrush = new SolidBrush(Color.Purple);
            SolidBrush grayBrush = new SolidBrush(Color.DimGray);
            SolidBrush blueBrush = new SolidBrush(Color.LightBlue);
            float px = (float)this.ballPos.X - ballRadius;
            float py = (float)this.ballPos.Y - ballRadius;

            e.Graphics.FillEllipse(purpleBrush, px, py, this.ballRadius * 2, this.ballRadius * 2);
            e.Graphics.FillRectangle(grayBrush, paddlePos);

            for (int i = 0; i < this.blockPos.Count; i++)
            {
                e.Graphics.FillRectangle(blueBrush, blockPos[i]);
            }
        }

        private void KeyPressed(object sender, KeyPressEventArgs e)
        {
            //キーが押されたときに発動するPressイベントハンドラ
            if(e.KeyChar == 'a')
            {
                this.paddlePos.X -= 20;
            }
            else if(e.KeyChar == 's')
            {
                this.paddlePos.X += 20;
            }
        }
    }
}
